#include <AccelStepper.h>
#include <MultiStepper.h>

AccelStepper stepper1(1,3,6); 
AccelStepper stepper2(1,4,7); 
// Up to 10 steppers can be handled as a group by MultiStepper
MultiStepper steppers;


const byte enablePin = 8;
String readString;

void setup() {

  pinMode(enablePin, OUTPUT);
  digitalWrite(enablePin, LOW);  
  
  stepper1.setMaxSpeed(1000.0);
  stepper1.setAcceleration(2000.0);

  stepper2.setMaxSpeed(1000.0);
  stepper2.setAcceleration(2000.0);

  // Then give them to MultiStepper to manage
  steppers.addStepper(stepper1);
  steppers.addStepper(stepper2);

      
  // start serial port at 9600 bps:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  pinMode(2, INPUT);   // digital sensor is on digital pin 2
  
}

int stepper1_angle;
int stepper2_angle;
int stepper3_angle;
int stepper4_angle;

void loop() {

  readString="";
  
  while (Serial.available()) {
    delay(3);  //delay to allow buffer to fill 
    if (Serial.available() >0) {
      char c = Serial.read();  //gets one byte from serial buffer
      readString += c; //makes the string readString
    } 
  }
  if (readString.length() >0) {
      
      // expect a string like 07002100 containing the two servo positions      
      stepper1_angle = readString.substring( 0,  4).toInt(); //get the first four characters
      stepper2_angle = readString.substring( 4,  8).toInt(); //get the next four characters 
      stepper3_angle = readString.substring( 8, 12).toInt(); //get the next four characters 
      stepper4_angle = readString.substring(12, 16).toInt(); //get the next four characters       

      Serial.println(stepper1_angle);
      Serial.println(stepper2_angle);
      Serial.println(stepper3_angle);
      Serial.println(stepper4_angle);                  
      Serial.println(readString); //see what was received
      readString="";

  }
}
