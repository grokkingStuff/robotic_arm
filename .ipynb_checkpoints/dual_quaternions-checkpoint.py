import numpy as np




class DualQuaternion:

    def __init__(self, dq_list):
        self.dq_list = dq_list

    def get_real(self):
        """Returns real part of dual quaternions"""
        return self.dq_list[:4]
    
    def get_dual(self):
        """Returns dual part of dual quaternions"""        
        return self.dq_list[4:]
    
    def __repr__(self):
        return f"Real Part: {self.get_real()}, Dual Part: {self.get_dual()}"    

#    def get_translation_vector(self):
#        """Returns translation vector from dual quaternion"""                
#        return self.dual[1:]

    @classmethod
    def from_real_and_dual(cls, real_part, dual_part):
        """Returns dual quaternion from real and dual quaternions"""        
        return cls(real_part + dual_part)
        
    @classmethod
    def from_scalar_and_vector(cls, real_scalar, real_vector, 
                                    dual_scalar, dual_vector):
        """Returns dual quaternion from real and dual scalar and vectors"""                
        real_part = np.array([real_scalar] + real_vector, dtype=float)
        dual_part = np.array([dual_scalar] + dual_vector, dtype=float)
        return cls.from_real_and_dual(real_part, dual_part)
        
    @classmethod
    def from_translation(cls, translation_vector):
        """Returns dual quaternion from translation vector"""                        
        real_part = np.array([1, 0, 0, 0], dtype=float)
        dual_part = np.array([0] + translation_vector * 0.5, dtype=float)
        return cls.from_real_and_dual(real_part, dual_part)

    @classmethod
    def from_rotation(cls, rotation_axis, angle_radians):
        """Returns dual quaternion from rotation vector and angle"""                                
        half_angle = angle_radians / 2
        real_scalar = np.cos(half_angle)
        real_vector = np.sin(half_angle) * rotation_axis / norm(rotation_axis)
        dual_scalar = 0
        dual_vector = np.zeros(3)
        return cls.from_scalar_and_vector(real_scalar, real_vector, dual_scalar, dual_vector)

    
        


    #def __mul__(self, other):
    #    real_part = self.real * other.real
    #    dual_part = self.real * other.dual + self.dual * other.real
    #    return DualQuaternion(real_part, dual_part)

    #def conjugate(self):
    #    return DualQuaternion(self.real.conj(), -self.dual.conj())

    #def magnitude(self):
    #    return np.sqrt(np.sum(self.real ** 2 + self.dual ** 2))

# Example usage:
#if __name__ == "__main__":
#    dq1 = DualQuaternion([1, 2, 3, 4], [5, 6, 7, 8])
#    dq2 = DualQuaternion([9, 10, 11, 12], [13, 14, 15, 16])##
#
#    print("Dual Quaternion 1:", dq1)
#    print("Dual Quaternion 2:", dq2)
#
#    print("\nMultiplication:")
#    result = dq1 * dq2
#    print("Result:", result)
#    print("\nConjugate:")
#    conjugate_dq1 = dq1.conjugate()
#    print("Conjugate of dq1:", conjugate_dq1)
#    print("\nMagnitude:")
#    print("Magnitude of dq1:", dq1.magnitude())
