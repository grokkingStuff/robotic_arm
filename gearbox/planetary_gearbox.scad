use <BOSL/nema_steppers.scad>
use <BOSL/threading.scad>
use <BOSL/metric_screws.scad>
use <publicDomainGearV1.3_trinarylogic.scad>

// Required Openscad libraries, check thank list below.

// --- Many thanks to Revar Desmera, the creator of the Openscad BOSL library - https://github.com/revarbat/BOSL
// --- As well as to Trinary Logic for the Public Domain Involute Parameterized Gears: Powered Up library-  https://www.thingiverse.com/thing:2854963

// Title : Compound planetary gearbox by Rodryk (2020) - v2.0

// ** Known bugs **
// Tolerance = 0.2 mm - failed rendering in the planet gears
// Use of design offset to remove preview artifacts

// ** Good gear ratios - number of teeth **
// Gear ratio 4:1 - S14, P14, R36
// Gear ratio 5:1 - S10, P15, R40


// *** Basic configuration ***


/* [Basic parameters] */

// Preview - 50; Render - 100
Resolution = 100; // [200]
// Half your 3d printer's usual tolerance. Good value are 0.15 to 0.3 mm.
tolerance = 0.15; // [0:0.05:0.35]

// Gears - Default settings - 5:1 gear ratio // Check http://www.thecatalystis.com/gears/ to simulate different gear ratios

/* [Gear configuration] */

sun_gear_number_of_teeth = 10;
planet_gear_number_of_teeth = 15;
ring_gear_number_of_teeth = 40;

// Multiply the gear ratio - Ex: 1 stage = 5:1; 2 stages = 25:1; 3 stages = 125:1; 4 stages = 625:1
ring_gear_number_stages = 1; // [1:1:4]

// Gear backlash - mm
backlash = 0.2; // [0:0.01:0.3]

gear_mm_per_tooth = 3; // [1:0.01:10]
gear_height = 6; // [1:0.01:20]

// Adjusts the radius of the bushings for planet gears as well the carrier top connections points
planet_gear_shaft_radius = 2; // [1:0.1:6]

// Adjusts the radius of the output shaft
output_shaft_adjustment = 1.5;

// Nuts and screws (M3 nut and screw used by default - bigger motor might be useful to use bigger nuts and screws

/* [Nuts and bolts] */

// Screw threaded part diameter - mm
screw_size = 3;
// Full screw height - mm
screw_length = 12;

// Nema motor parameters - Default setting for Nema 17 model:

/* [Nema motor] */

// Nema type
nema_stepper_size = 17; // [11,14,17,23,34]
// Nema motor output shaft. Check manufacturer's data sheet. - mm
nema_stepper_shaft_height = 18.3; // [0:0.1:50]
// Nema motor output shaft radius - mm
nema_stepper_shaft_radius = 2.4; // [0:0.01:10]
// Nema stepper motor body height. - mm
nema_stepper_height = 47;

/* [Nema motor - D_CUT_shaft] */
D_CUT_shaft_length = 5; // [0:0.1:40]
D_CUT_shaft_width = 5; // [0:0.1:40]
D_CUT_shaft_height = 18.3; // [0:0.1:40]
D_CUT_shaft_x_offset = 2; // [0:0.1:40]
D_CUT_shaft_y_offset = 2.5; // [0:0.1:40]
D_CUT_shaft_z_offset = 1.5; // [0:0.1:40]

/* [Ball bearing components] */

// Ball bearing

// Snap fit for a 4.3 mm diameter bearing ball. Example: 4.3 / 2 = 2.15 mm + 0.2 mm tolerance = 2.35 mm ball radii
Ball_radius = 2.35; // [2.2:0.01:2.5]

/* [Hidden] */

// *** Advanced configuration - Warning: " There be dragons here! " ***


// Parameters - (All values are in milimeters)

// Gear general settings

gear_twist = 0; // Rotate teeth 0-360º from bottom of the gear to the top. Not implemented fully.

// *** Variables ***

$fn=Resolution; // Don't mess with it!
design_offset = 1; // Don't mess with it!. Fix openscad bug in preview mode. Doesn't affect final render.
warp_factor_adjustment = 0.025; // ABS - 0.025 to 0.05

// Nema stepper motor
nema_stepper_shaft_attachment_radius = outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth); // Don't mess with it!

// Planet gear - Default settings for 5:1 gear ratio
planet_gear_quantity = 3;  // Number of planet gears in gearbox stage.

// Threading
threading_height = gear_height/2; // Don't mess with it. Calculates height of thread used to connect the different parts fo the gearbox
threading_bottom_offset = 5; // Don't mess with it. Used to create the bottom threading and attachment points to connect motor to a support structure
threading_pitch = 2; // Don't mess with it. Length between threads in mm.

// Ring gear - Default settings for 1:5 gear ratio
ring_gear_thickness = (nema_motor_width(size=nema_stepper_size)-2*outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=ring_gear_number_of_teeth))/2; // Variable used in modules below
ring_gear_chamfer_angle_adjustment = tan(65)+ring_gear_thickness-threading_height; // Don't mess with it.

// Carrier
carrier_radius_spacer = 0.5; // Don't mess with it. Space between planet gear and their enclosure
carrier_radius = root_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=ring_gear_number_of_teeth)-carrier_radius_spacer; // Calculates the radius of the carrier

// Gearbox cap
cap_threading_thickness_adjustment = 2;

// Output shaft
output_shaft_radius = output_shaft_adjustment*outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth= sun_gear_number_of_teeth); // Calculate the final gear output shaft radius


// Ball bearings - Advanced - used an adaptation of the parametric ball bearing -

Ball_bearing_radius = carrier_radius;
Ball_bearing_height = gear_height;
Number_of_balls = 10; // Decrease or increase if you see artifacts in the ball bearing carrier.
Thrust_ball_bearing_ball_cage_height = 1.5; // Don't mess with it
Thrust_bearing_gap_height = 2; // Don't mess with it
Thrust_bearing_groove_radius = 3/4*carrier_radius;
Radial_ball_bearing_shaft_radius = output_shaft_radius;
Radial_bearing_gap_radius = 3/4*carrier_radius;
Radial_ball_bearing_ball_cage_radius = 1.6;// Attention! Sketch mentions a diameter. This is a radius
Radial_ball_bearing_cap_thickness = 1; // Don't mess with it. Generates a cap for the inside of the ball bearing


/* [Preview] */

// ** Preview section**

// 0 - Don't show preview; 1 - Compact view = false; Expanded view = 2;
view_mode = 0; // [0,1,2]
preview(view_mode);


/* [Render parts] */

// ** Generate individual parts ** - Note: You'll need at least one of each parts.

nema_motor_plate = false;
generate_nema_motor_plate(nema_motor_plate);

nema_motor_plate_attachment = false;
generate_nema_motor_plate_attachment(nema_motor_plate_attachment);

gearbox_case = false;
generate_gearbox_case(gearbox_case);

gearbox_cap = false;
generate_gearbox_cap(gearbox_cap);

upper_ball_bearing_cap = false;
generate_upper_ball_bearing_cap(upper_ball_bearing_cap);

bottom_ball_bearing_cap = false;
generate_bottom_ball_bearing_cap(bottom_ball_bearing_cap);

nema_gear = false;
generate_nema_gear(nema_gear);

carrier = false;
generate_carrier(carrier);

planet_gear = false;
generate_planet_gear(planet_gear);

sun_gear_carrier = false;
generate_sun_gear_carrier(sun_gear_carrier);

output_shaft = false;
generate_output_shaft(output_shaft);

ball_bearing = false;
generate_ball_bearing(ball_bearing);

output_ball_bearing = false;
generate_output_ball_bearing(output_ball_bearing);

ball_bearing_cage = false;
generate_ball_bearing_cage(ball_bearing_cage);

output_ball_bearing_cage = false;
generate_output_ball_bearing_cage(output_ball_bearing_cage);

// Generation modules

// Nema motor plate
module generate_nema_motor_plate(nema_motor_plate)
{
    if (nema_motor_plate == true)
        nema_motor_plate();
}

// Nema motor plate attachment
module generate_nema_motor_plate_attachment(nema_motor_plate_attachment)
{
    if (nema_motor_plate_attachment == true)
        nema_motor_plate_attachment();
}

// Gearbox case
module generate_gearbox_case(gearbox_case)
{
    if (gearbox_case == true)
        gearbox_case();
}

// Gearbox cap - Rotate 180º in slicer
module generate_gearbox_cap(gearbox_cap)
{
    if (gearbox_cap == true)
        gearbox_cap();
}

// Radial ball bearing caps
module generate_upper_ball_bearing_cap(upper_ball_bearing_cap)
{
    if (upper_ball_bearing_cap == true)
        radial_ball_bearing_upper_cap(3/2*gear_height);
}

module generate_bottom_ball_bearing_cap(bottom_ball_bearing_cap)
{
    if (bottom_ball_bearing_cap == true)
        radial_ball_bearing_bottom_cap(3/2*gear_height);
}

// Motor sun gear - Note: This part needs a brim on the center piece.
module generate_nema_gear(nema_gear)
{
    if (nema_gear == true)
        nema_gear();
}

// Carrier
module generate_carrier(carrier)
{
    if (carrier == true)
        carrier();
}

// Planet gears
module generate_planet_gear(planet_gear)
{
    if (planet_gear == true)
        planet_gear();
}

// Sun gear carrier
module generate_sun_gear_carrier(sun_gear_carrier)
{
    if (sun_gear_carrier == true)
        sun_gear_carrier();
}

// Gearbox shaft
module generate_output_shaft(output_shaft)
{
    if (output_shaft == true)
        output_shaft();
}

// Thrust ball bearing - Cut the pieces in two in the slicer
module generate_ball_bearing(ball_bearing)
{
    if (ball_bearing == true)
        ball_bearing();
}

// Output ball bearing - Cut the pieces in two in the slicer
module generate_output_ball_bearing(output_ball_bearing)
{
    if (output_ball_bearing)
        output_ball_bearing();
}

// Thrust ball bearing cage
module generate_ball_bearing_cage(ball_bearing_cage)
{
    if (ball_bearing_cage == true)
        ball_bearing_cage();
}

// Output ball bearing cage
module generate_output_ball_bearing_cage(output_ball_bearing_cage)
{
    if (output_ball_bearing_cage == true)
        output_ball_bearing_cage();
}




// *** Procedural generation modules ***

// Sun gear
module sun_gear()
    {
        // Create sun gear
        gear(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth, thickness=gear_height, hole_diameter=0, twist=-gear_twist, backlash=backlash, clearance=tolerance);
    }

// Planet gear
module planet_gear()
    {
        // Central location between pitch radius of sun gear and ring gear
        x=pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=ring_gear_number_of_teeth)-pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=planet_gear_number_of_teeth);

        // Position each planet gear
        for(angle_rotation = [0:360/planet_gear_quantity:360])
            {
                // Create planet gear in the adequate position while adjusting it to material warp
                rotate([0,0,angle_rotation]) translate([x,0,0]) scale([1,1,1]) gear(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=planet_gear_number_of_teeth, thickness=gear_height, hole_diameter=2*(planet_gear_shaft_radius+tolerance/2), twist=gear_twist, backlash=backlash,clearance=tolerance);
        }
    }

// Nema motor attachment to planetary gear
module nema_motor_plate()
{
    gearbox_radius = ( sqrt( pow(nema_motor_width(size=nema_stepper_size),2) + pow(nema_motor_width(size=nema_stepper_size),2) ) ) / 2;

    // Create nema motor attachment plate, also connect to gearbox
    difference()
    {
        // Create main body
        union()
        {
            // Create inner body
            difference()
            {
                // Create main inner body
                cylinder(nema_stepper_shaft_height/2+nema_motor_plinth_height(size=nema_stepper_size),gearbox_radius+threading_bottom_offset,gearbox_radius+threading_bottom_offset);

                // Create inside hole for sun gear connection to motor shaft
                translate([0,0,-nema_motor_plinth_height(size=nema_stepper_size)-design_offset]) cylinder(nema_stepper_shaft_height+design_offset,nema_motor_plinth_diam(size=nema_stepper_size)/2+tolerance,nema_motor_plinth_diam(size=nema_stepper_size)/2+tolerance);

                // Create inside space for nema gear ball bearings
                translate([0,0,nema_motor_plinth_height(size=nema_stepper_size)]) cylinder(nema_stepper_shaft_height-nema_motor_plinth_height(size=nema_stepper_size)+design_offset,carrier_radius+tolerance,carrier_radius+tolerance);

                // Create screw holes to attach case to nema motor

                // Nema motor screw spacing
                w = nema_motor_screw_spacing(size=nema_stepper_size);

                // Create all 4 holes, according to nema motor size
                for(x = [-w/2:w:w/2])
                {
                    for(y = [-w/2:w:w/2])
                    {
                        translate([x,y,-nema_motor_plinth_height(size=nema_stepper_size)-design_offset]) cylinder(nema_motor_plinth_height(size=nema_stepper_size)+nema_stepper_shaft_height/2+2*design_offset,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance);

                        translate([x,y,screw_length/2]) cylinder(gear_height+design_offset,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance+2,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance+2);
                    }
                }
            }
        }
    }

    // Create threading part of nema plate motor
    union()
    {
        // Create threading to attach to gearbox case
        intersection()
        {
            // Create main body
            translate([0,0,nema_stepper_shaft_height/2+nema_motor_plinth_height(size=nema_stepper_size)]) cylinder(threading_height,gearbox_radius+threading_bottom_offset,gearbox_radius+threading_bottom_offset);

            // Inner threads to connect to gearbox case
            translate([0,0,nema_stepper_shaft_height/2+nema_motor_plinth_height(size=nema_stepper_size)+threading_height/2]) acme_threaded_nut(od=2*(gearbox_radius+threading_bottom_offset), id=2*(gearbox_radius+tolerance), h=threading_height, pitch=threading_pitch, thread_angle=30, slop=0.2);
        }

        // Create support structure attachment to motor
        intersection()
        {
            difference()
            {
                // Main body
                cylinder(nema_stepper_shaft_height/2+nema_motor_plinth_height(size=nema_stepper_size),gearbox_radius+threading_bottom_offset+10,gearbox_radius+threading_bottom_offset+5);

                // Remove interior
                translate([0,0,-design_offset]) cylinder(nema_stepper_shaft_height/2+nema_motor_plinth_height(size=nema_stepper_size)+2*design_offset,gearbox_radius+threading_bottom_offset,gearbox_radius+threading_bottom_offset);

                // Create screw holes to attach gearbox to support structure

                // Nema motor screw spacing
                w = gearbox_radius+threading_bottom_offset+5;

                // Create all 4 holes, according to nema motor size
                for(rot = [45:90:360])
                {
                    rotate([0,0,rot])
                    {
                        translate([w,0,0]) cylinder(2*gear_height,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance);

                        translate([w,0,screw_length/2]) cylinder(2*gear_height+design_offset,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance+2,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance+2);
                    }
                }
            }

            w = gearbox_radius+threading_bottom_offset;
            for(rot = [45:90:360])
            {
                rotate([0,0,rot])
                {
                    translate([w,0,0]) cylinder(2*gear_height,10,10);
                }
            }
        }
    }
}

module nema_motor_plate_attachment()
{
    gearbox_radius = ( sqrt( pow(nema_motor_width(size=nema_stepper_size),2) + pow(nema_motor_width(size=nema_stepper_size),2) ) ) / 2;

    attachment_height = 7.5;

    w = gearbox_radius+threading_bottom_offset;

    // Create main body
    difference()
    {
        // Main body attachment
        cylinder(attachment_height,gearbox_radius+threading_bottom_offset+10,gearbox_radius+threading_bottom_offset+10);

        // Remove inside body attachment
        translate([0,0,-design_offset]) cylinder(attachment_height+2*design_offset,gearbox_radius+threading_bottom_offset,gearbox_radius+threading_bottom_offset);


        // Place holes for attachment
        for(rot = [45:90:405])
        {
            rotate([0,0,rot])
            {
                translate([w+5,0,0]) cylinder(attachment_height+design_offset,nema_motor_screw_size(size=nema_stepper_size)/2-tolerance,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance);

                translate([w+5,0,attachment_height-2]) cylinder(2+design_offset,nema_motor_screw_size(size=nema_stepper_size)/2+1.5,nema_motor_screw_size(size=nema_stepper_size)/2+1.5);
            }
        }
    }

    // Create support connectors
    difference()
    {
        for(rot = [0:90:360])
        {
            rotate([0,0,rot])
            {
                translate([w,0,0]) cylinder(nema_stepper_height+2*tolerance,10,10);
            }
        }

        for(rot = [0:90:360])
        {
            // Nema motor screw spacing
            w = gearbox_radius+threading_bottom_offset+5;

            rotate([0,0,rot])
            {
                // Place screw holes on support pillars
                translate([w,0,nema_stepper_height-2*gear_height]) cylinder(2*gear_height+design_offset,nema_motor_screw_size(size=nema_stepper_size)/2,nema_motor_screw_size(size=nema_stepper_size)/2+tolerance);

                // Place M3 nut hole on top of pillars
                translate([w,0,nema_stepper_height-2.1*1.5]) scale([0.99,0.99,1.5]) metric_nut(size=3, hole=false);
            }
        }

        // Remove inside of support pillars
        translate([0,0,-design_offset]) cylinder(nema_stepper_height+2*design_offset,gearbox_radius+threading_bottom_offset,gearbox_radius+threading_bottom_offset);
    }
}

// Ring gear upper half - Component of the gearbox case
module ring_gear_upper_half()
    {
        gearbox_radius = ( sqrt( pow(nema_motor_width(size=nema_stepper_size),2) + pow(nema_motor_width(size=nema_stepper_size),2) ) ) / 2;

        // Create ring gear upper half
        difference()
        {
            // Create main body
            union()
            {
                // Create gearbox main body
                cylinder(3/2*gear_height,gearbox_radius,gearbox_radius);

                // Create threading to connect to gearbox cap
                translate([0,0,3/2*gear_height+threading_height/2]) acme_threaded_rod(d=2*(gearbox_radius-ring_gear_thickness-cap_threading_thickness_adjustment-tolerance), l=threading_height, pitch=threading_pitch, thread_angle=30, $fn=100);
            }

            // Remove inside of threaded region
            translate([0,0,3/2*gear_height]) cylinder(threading_height+design_offset,outer_radius(mm_per_tooth = gear_mm_per_tooth, number_of_teeth = ring_gear_number_of_teeth),outer_radius(mm_per_tooth = gear_mm_per_tooth, number_of_teeth = ring_gear_number_of_teeth));
        }
    }

// Ring gear bottom half - Component of the gearbox case
module ring_gear_bottom_half()
{
    gearbox_radius = ( sqrt( pow(nema_motor_width(size=nema_stepper_size),2) + pow(nema_motor_width(size=nema_stepper_size),2) ) ) / 2;

    // Create ring gear bottom half
    difference()
    {
        // Rotate downward the upper half ring gear
        rotate([0,180,0]) difference()
        {
            // Create main body
            union()
            {
                // Create gearbox main body
                cylinder(5/2*gear_height-threading_height,gearbox_radius,gearbox_radius);

                // Create threading to connect to gearbox cap
                translate([0,0,5/2*gear_height-threading_height/2]) acme_threaded_rod(d=2*(gearbox_radius-tolerance), l=threading_height, pitch=threading_pitch, thread_angle=30, $fn=100);
            }
        }
    }
}

// Gearbox case
module gearbox_case()
{
    // Variable that calculates the height of the gearbox
    gearbox_calc_height = 3*threading_height+4*gear_height+(ring_gear_number_stages-1)*(3*gear_height);

    // Create gearbox case
    difference()
    {
        // Create outer shell of gearbox
        union()
        {
            // Generate first ring gear stage
            translate([0,0,5/2*gear_height]) ring_gear_bottom_half(); // Bottom ring gear
            translate([0,0,5/2*gear_height]) ring_gear_upper_half(); // Upper ring gear

            // Generate all ring gear stages until the last one, starting at the end of the first stage
            translate([0,0,threading_height+3.5*gear_height]) for(number_of_stages = [0:1:ring_gear_number_stages-2])
            {

                translate([0,0,number_of_stages*(3*gear_height)]) union()
                {
                    ring_gear_upper_half();
                    translate([0,0,3/2*gear_height]) ring_gear_upper_half();
                }
            }
        }

        // Create inner gear - Note: Since this gear is created by difference. Gear twist is the opposite value
        translate([0,0,gearbox_calc_height/2]) scale([1+warp_factor_adjustment,1+warp_factor_adjustment,1+warp_factor_adjustment]) gear(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=ring_gear_number_of_teeth, thickness=gearbox_calc_height, hole_diameter=0, twist=gear_twist, backlash=backlash);
    }
}

// Carrier
module carrier()
    {
        // Create carrier bottom body
        difference()
        {
            // Main body
            cylinder(gear_height/2,carrier_radius-tolerance,carrier_radius-tolerance);

            // Create hole for sun gear
            translate([0,0,-design_offset]) cylinder(gear_height/2+2*design_offset,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+carrier_radius_spacer,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+carrier_radius_spacer);
        }

        // Create carrier upper body
        difference() {

            // Main body
            translate([0,0,gear_height/2]) cylinder(gear_height,carrier_radius-tolerance,carrier_radius-tolerance);

            // Create holes for planet gears

            // Variable - X coordinates of planet gear center - Central location between pitch radius of sun gear and ring gear
            x=(pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=ring_gear_number_of_teeth))/2;

            // Create inset for each planet gear
            for(angle_rotation = [0:360/planet_gear_quantity:360])
            {
                rotate([0,0,angle_rotation]) translate([x,0,gear_height/2-design_offset]) cylinder(gear_height+2*design_offset,outer_radius(mm_per_tooth=gear_mm_per_tooth,number_of_teeth=planet_gear_number_of_teeth)+carrier_radius_spacer,outer_radius(mm_per_tooth=gear_mm_per_tooth,number_of_teeth=planet_gear_number_of_teeth)+carrier_radius_spacer);
            }

            // Create inset for sun gear attachment
            translate([0,0,gear_height/2-design_offset]) cylinder(gear_height+2*design_offset,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+carrier_radius_spacer,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+carrier_radius_spacer);
        }

        // Variable - X coordinates of planet gear center - Central location between pitch radius of sun gear and ring gear
        x=(pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=ring_gear_number_of_teeth))/2;

        // Create shafts to rotate planet gears
        for(angle_rotation = [0:360/planet_gear_quantity:360])
        {
            rotate([0,0,angle_rotation]) translate([x,0,gear_height/2]) cylinder(gear_height,planet_gear_shaft_radius-tolerance/2,planet_gear_shaft_radius-tolerance/2);
        }

        // Create shafts to connect motion to upper sun gear attachment
        for(angle_rotation = [(360/planet_gear_quantity)/2:360/planet_gear_quantity:360+(360/planet_gear_quantity)/2])
        {
            rotate([0,0,angle_rotation]) translate([x,0,1.5*gear_height]) cylinder(gear_height/2,1.5*planet_gear_shaft_radius-tolerance/2,1.5*planet_gear_shaft_radius-tolerance/2);
        }
    }

// Sun gear carrier - connects bottom carrier to next carrier of planet gears
module sun_gear_carrier()
{
    // Create sun gear carrier
    difference()
    {
        difference()
        {
            // Create main body
            union()
            {
                // Create sun gear
                scale([1,1,1]) translate([0,0,5/2*gear_height]) sun_gear();
                // Create middle plate
                translate([0,0,gear_height/2]) cylinder(3/2*gear_height,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth= sun_gear_number_of_teeth),outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth= sun_gear_number_of_teeth));
                // Create bottom plate
                cylinder(gear_height/2,carrier_radius-tolerance,carrier_radius-tolerance);
            }

            // Create attachment holes
            // Central location between pitch radius of sun gear and ring gear
            x=(pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=ring_gear_number_of_teeth))/2;
            for(angle_rotation = [(360/planet_gear_quantity)/2:360/planet_gear_quantity:360+(360/planet_gear_quantity)/2])
            {
                rotate([0,0,angle_rotation]) translate([x,0,-design_offset]) cylinder(gear_height/2+2*design_offset,1.5*planet_gear_shaft_radius,1.5*planet_gear_shaft_radius);
            }
        }

        // Hollow center - Improves printing quality, since slicer doesn't produce infill
        cylinder(2*gear_height,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth= sun_gear_number_of_teeth)-2,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth= sun_gear_number_of_teeth)-2);
    }
}

// Sun gear nema attachment
module nema_gear()
{
    // Create sun gear nema attachment
    difference()
    {
        // Create main body
        union()
        {
            // Calculate the height of the bottom of the nema gear
            bottom_gear_height = 3/2*gear_height+gear_height+gear_height/2;

            // Create shaft attachment body
            cylinder(bottom_gear_height,nema_stepper_shaft_attachment_radius,nema_stepper_shaft_attachment_radius);

            // Create main sun gear and adjust to ABS warping
            translate([0,0,bottom_gear_height+gear_height/2]) scale([1,1,1]) sun_gear();

            // Create radial ball bearing

              // Adjusting paramenters
              Ball_bearing_height=3/2*gear_height;
              Shaft_radius = Radial_ball_bearing_shaft_radius;

            rotate([180,0,0]) radial_ball_bearing(Ball_bearing_height,Shaft_radius);
        }

        // Create shaft hole - Adjust these to fine tune the fit between motor shaft and nema gear
        difference()
        {

            translate([0,0,-design_offset]) cylinder(nema_stepper_shaft_height+2*design_offset+2*tolerance,nema_stepper_shaft_radius+tolerance,nema_stepper_shaft_radius+tolerance);
            translate([D_CUT_shaft_x_offset+tolerance/2,-D_CUT_shaft_y_offset,D_CUT_shaft_z_offset]) cube([D_CUT_shaft_length,D_CUT_shaft_width,D_CUT_shaft_height]);
        }
    }
}

// Thrust ball bearing
module ball_bearing()
{
    // Thrust ball bearing
    module thrust_ball_bearing()
    {

        // Create main body of bearing
        difference()
        {
            // Create main body
            cylinder(Ball_bearing_height,Ball_bearing_radius,Ball_bearing_radius);
            // Creates gap in ball bearing
            translate([0,0,Ball_bearing_height/2]) cylinder(Thrust_bearing_gap_height,Ball_bearing_radius+design_offset,Ball_bearing_radius+design_offset,center=true);
            // Creates ball groove
            translate([0,0,Ball_bearing_height/2]) rotate_extrude(angle=360) translate([Thrust_bearing_groove_radius,0,0]) circle(Ball_radius);
        }
    }

    // Create thrust ball bearing
    difference()
    {
        // Create main body
        thrust_ball_bearing();
        // Create hole for motor shaft
        cylinder(gear_height,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+2*tolerance,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+2*tolerance);
    }
}

// Output shaft thrust ball bearing
module output_ball_bearing()
{
    // Thrust ball bearing
    module thrust_ball_bearing()
    {

        // Create main body of bearing
        difference()
        {
            // Create main body
            cylinder(Ball_bearing_height,Ball_bearing_radius,Ball_bearing_radius);
            // Creates gap in ball bearing
            translate([0,0,Ball_bearing_height/2]) cylinder(Thrust_bearing_gap_height,Ball_bearing_radius+design_offset,Ball_bearing_radius+design_offset,center=true);
            // Creates ball groove
            translate([0,0,Ball_bearing_height/2]) rotate_extrude(angle=360) translate([Thrust_bearing_groove_radius,0,0]) circle(Ball_radius);
        }
    }

    // Create thrust ball bearing
    difference()
    {
        // Create main body
        thrust_ball_bearing();
        // Create hole for motor shaft
        cylinder(gear_height,output_shaft_radius,output_shaft_radius);
    }
}

// Thrust ball bearing cage
module ball_bearing_cage()
{
    // Function to obtain the position of ball bearings
    function 3d_circle(t) = [
        (Thrust_bearing_groove_radius*cos(t)),
        (Thrust_bearing_groove_radius*sin(t)),
        Ball_bearing_height/2
        ];
    // Matrix with the coordinates of all the balls
    shape_points = [ for(i = [0:$fn/Number_of_balls:$fn]) let (a=i*360/$fn) 3d_circle(a)];

    // Create bearing cage
    difference()
    {
        // Create cage ring
        translate([0,0,Ball_bearing_height/2]) cylinder(Thrust_ball_bearing_ball_cage_height,Ball_bearing_radius,Ball_bearing_radius,center=true);

        // Create all the balls
        for(i = [0:len(shape_points)-1]) translate(shape_points[i]) sphere(Ball_radius);

        // Create shaft
        translate([0,0,Ball_bearing_height/2]) cylinder(Thrust_ball_bearing_ball_cage_height+design_offset,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+2*tolerance,outer_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+2*tolerance,center=true);
    }
}

// Output shaft thrust ball bearing cage
module output_ball_bearing_cage()
{
    // Function to obtain the position of ball bearings
    function 3d_circle(t) = [
        (Thrust_bearing_groove_radius*cos(t)),
        (Thrust_bearing_groove_radius*sin(t)),
        Ball_bearing_height/2
        ];
    // Matrix with the coordinates of all the balls
    shape_points = [ for(i = [0:$fn/Number_of_balls:$fn]) let (a=i*360/$fn) 3d_circle(a)];

    // Create bearing cage
    difference()
    {
        // Create cage ring
        translate([0,0,Ball_bearing_height/2]) cylinder(Thrust_ball_bearing_ball_cage_height,Ball_bearing_radius,Ball_bearing_radius,center=true);

        // Create all the balls
        for(i = [0:len(shape_points)-1]) translate(shape_points[i]) sphere(Ball_radius);

        // Create shaft
        translate([0,0,Ball_bearing_height/2-design_offset]) cylinder(Thrust_ball_bearing_ball_cage_height+2*design_offset,output_shaft_radius,output_shaft_radius);
    }
}

// Output shaft
module output_shaft()
{
    // Create output shaft carrier
    difference()
    {
        difference()
        {
            // Create main body
            union()
            {
                // Create shaft main body
                translate([0,0,5/2*gear_height]) cylinder(0.5*gear_height,output_shaft_radius,output_shaft_radius);

                // Create spool attachment
                difference()
                {
                    // Create main attachment body
                    rotate([0,0,45]) translate([0,0,(3+5/2)*gear_height]) cube([sqrt((pow(2*output_shaft_radius-tolerance,2))/2),sqrt((pow(2*output_shaft_radius-tolerance,2))/2),5*gear_height], center=true);

                    // Drill hole for M3x20 screw
                    translate([0,0,(3+5)*gear_height-10]) cylinder(10,1.5,1.5);
                }

                // Create middle plate
                translate([0,0,gear_height/2]) cylinder(2*gear_height,output_shaft_radius,output_shaft_radius);
                // Create bottom plate
                cylinder(gear_height/2,carrier_radius-tolerance,carrier_radius-tolerance);
            }

            // Create attachment holes

            // Central location between pitch radius of sun gear and ring gear
            x=(pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=sun_gear_number_of_teeth)+pitch_radius(mm_per_tooth=gear_mm_per_tooth, number_of_teeth=ring_gear_number_of_teeth))/2;

            for(angle_rotation = [(360/planet_gear_quantity)/2:360/planet_gear_quantity:360+(360/planet_gear_quantity)/2])
            {
                rotate([0,0,angle_rotation]) translate([x,0,-design_offset]) cylinder(gear_height/2+2*design_offset,1.5*planet_gear_shaft_radius,1.5*planet_gear_shaft_radius);
            }
        }

        // Hollow center - Note: Slicer's don't infill for some reason
        cylinder(2.8*gear_height,output_shaft_radius-2,output_shaft_radius-2);
    }
}

// Radial ball bearing
module radial_ball_bearing(Ball_bearing_height,Radial_ball_bearing_shaft_radius)
{
    // Function to obtain the position of ball bearings
    function 3d_circle(t) = [
        (Radial_bearing_gap_radius*cos(t)),
        (Radial_bearing_gap_radius*sin(t)),
        Ball_bearing_height/2
        ];

    // Matrix with the coordinates of all the balls
    shape_points = [ for(i = [0:$fn/Number_of_balls:$fn]) let (a=i*360/$fn) 3d_circle(a)];

    // Create bearing shells
    rotate([180,0,0]) union()
    {
        // Create shells
        difference()
        {
            rotate_extrude(angle=360)
            {
                difference()
                {
                    union()
                    {
                        // Variable
                        inner_shell_offset = Radial_bearing_gap_radius-Radial_ball_bearing_shaft_radius-(Ball_radius/2);

                        // Inner shell
                        translate([Radial_ball_bearing_shaft_radius,0,0]) square([inner_shell_offset, Ball_bearing_height-Radial_ball_bearing_cap_thickness]);

                        // Outer shell
                        translate([Radial_bearing_gap_radius+(Ball_radius/2),0,0]) square([Ball_bearing_radius-Radial_bearing_gap_radius-(Ball_radius/2), Ball_bearing_height]);
                    }
                    // Ball bearing groove
                    translate([Radial_bearing_gap_radius, Ball_bearing_height/2, 0]) circle(Ball_radius);
                }
            }
            // Ball insertion hole
            translate([Radial_bearing_gap_radius,0,Ball_bearing_height/2]) cylinder(Ball_bearing_height/2+1,Ball_radius,Ball_radius);
        }
        // Create bearing cage
        difference()
        {
            // Create cage ring
            rotate_extrude(angle=360) translate([Radial_bearing_gap_radius-(Radial_ball_bearing_ball_cage_radius/2),0,0]) square([Radial_ball_bearing_ball_cage_radius, Ball_bearing_height/2 + 0.5*Ball_radius]);
            // Create all the balls
            for(i = [0:len(shape_points)-1]) translate(shape_points[i]) sphere(Ball_radius);
        }
    }
}

// Radial ball bearing upper cap
module radial_ball_bearing_upper_cap(Ball_bearing_height)
{
    // Creates upper cap
    difference()
    {
        // Creates main body of cap
        union()
        {
            // Create top of cap
            translate([0,0,Ball_bearing_height-Radial_ball_bearing_cap_thickness]) cylinder(Radial_ball_bearing_cap_thickness,Radial_bearing_gap_radius+Radial_ball_bearing_ball_cage_radius/2,Radial_bearing_gap_radius+Radial_ball_bearing_ball_cage_radius/2);

            // Create inner shaft attachment
            difference()
            {
                // Creates main body
                cylinder(Ball_bearing_height-Radial_ball_bearing_cap_thickness,Radial_ball_bearing_shaft_radius+Radial_ball_bearing_cap_thickness,Radial_ball_bearing_shaft_radius+Radial_ball_bearing_cap_thickness);
                // Removes shaft inside
                translate([0,0,-design_offset]) cylinder(Ball_bearing_height-Radial_ball_bearing_cap_thickness+design_offset,Radial_ball_bearing_shaft_radius,Radial_ball_bearing_shaft_radius);
            }
        }
        // Drills shaft hole through the top of cap
        cylinder(Ball_bearing_height+design_offset,Radial_ball_bearing_shaft_radius,Radial_ball_bearing_shaft_radius);
    }
}

// Radial ball bearing upper cap
module radial_ball_bearing_bottom_cap(Ball_bearing_height)
{
    // Creates bottom cap
    difference()
    {
        // Creates main body of cap
        union()
        {
            // Create top of cap
            translate([0,0,Ball_bearing_height-Radial_ball_bearing_cap_thickness]) cylinder(Radial_ball_bearing_cap_thickness,Radial_bearing_gap_radius+Radial_ball_bearing_ball_cage_radius/2,Radial_bearing_gap_radius+Radial_ball_bearing_ball_cage_radius/2);
            // Create inner shaft attachment
            difference()
            {
                // Creates main body
                cylinder(Ball_bearing_height-Radial_ball_bearing_cap_thickness,Radial_ball_bearing_shaft_radius-tolerance,Radial_ball_bearing_shaft_radius-tolerance);
                // Removes shaft inside
                translate([0,0,-design_offset]) cylinder(Ball_bearing_height-Radial_ball_bearing_cap_thickness+design_offset,nema_stepper_shaft_attachment_radius+tolerance,nema_stepper_shaft_attachment_radius+tolerance);
            }
        }
        // Drills shaft hole through the top of cap
        cylinder(Ball_bearing_height+design_offset,nema_stepper_shaft_attachment_radius+tolerance,nema_stepper_shaft_attachment_radius+tolerance);
    }
}

// Gearbox cap
module gearbox_cap()
{
    gearbox_radius = ( sqrt( pow(nema_motor_width(size=nema_stepper_size),2) + pow(nema_motor_width(size=nema_stepper_size),2) ) ) / 2;

    // Create output shaft cap
    union()
    {
        // Create upper part of output shaft cap with ball bearing
        union()
        {
            // Create radial ball bearing on top of cap
            Ball_bearing_height=3/2*gear_height;
            Shaft_radius = Radial_ball_bearing_shaft_radius + Radial_ball_bearing_cap_thickness + tolerance;

            translate([0,0,3/2*gear_height]) radial_ball_bearing(Ball_bearing_height,Shaft_radius);

            // Create output shaft cap
            difference()
            {
                // Create main body
                translate([0,0,gear_height/2]) cylinder(gear_height,gearbox_radius,gearbox_radius);

                // Remove space for output shaft ball bearing
                translate([0,0,-design_offset]) cylinder(3/2*gear_height+2*design_offset, Ball_bearing_radius, Ball_bearing_radius);
            }
        }

        // Create bottom part of output shaft with threading
        intersection()
        {
            // Create bottom part of output shaft
            difference()
            {
                // Create main body
                cylinder(gear_height/2,gearbox_radius,gearbox_radius);
                // Create hole for shaft
                translate([0,0,-design_offset]) cylinder(gear_height+2*design_offset, Ball_bearing_radius, Ball_bearing_radius);
            }
            // Inner threads to connect to gearbox case
            translate([0,0,threading_height/2]) acme_threaded_nut(od=2*(gearbox_radius), id=2*(gearbox_radius-ring_gear_thickness-cap_threading_thickness_adjustment+tolerance), h=threading_height, pitch=threading_pitch, thread_angle=30, slop=0.2);
        }
    }

}

// Preview
module preview(view_mode)
{
    if(view_mode == 0)
    {
    }

    else if(view_mode == 1)
    {
        // Generate stepper motor
        if(nema_stepper_size==11) {translate([0,0,-nema_motor_plinth_height(size=nema_stepper_size)]) nema11_stepper();}
        else if(nema_stepper_size==14) {translate([0,0,-nema_motor_plinth_height(size=nema_stepper_size)]) nema14_stepper();}
        else if(nema_stepper_size==17) {translate([0,0,-nema_motor_plinth_height(size=nema_stepper_size)]) nema17_stepper();}
        else if(nema_stepper_size==23) {translate([0,0,-nema_motor_plinth_height(size=nema_stepper_size)]) nema23_stepper();}
        else if(nema_stepper_size==34) {translate([0,0,-nema_motor_plinth_height(size=nema_stepper_size)]) nema34_stepper();}
        else{echo("*** Error: I'm only programmed to create planetary gears for nema steppers size 11,14,17,23 and 34 ***");}

        // Generate stepper motor sun
        nema_gear();

        // Generate bottom ball bearing cap
        radial_ball_bearing_bottom_cap(3/2*gear_height);

        // Generate nema motor attachment
        translate([0,0,-nema_motor_plinth_height(size=nema_stepper_size)]) nema_motor_plate();

        // Generate nema motor plate attachment
        rotate([0,0,45]) translate([0,0,-nema_stepper_height-nema_motor_plinth_height(size=nema_stepper_size)]) nema_motor_plate_attachment();

        // Generate gearbox case
        translate([0,0,nema_stepper_shaft_height/2-2*threading_height+gear_height]) gearbox_case();

        // Generate gearbox cap
        translate([0,0,nema_stepper_shaft_height-2*threading_height+ring_gear_number_stages*(3*gear_height)+gear_height/2]) gearbox_cap();

        // Generate upper ball bearing cap
        translate([0,0,nema_stepper_shaft_height-2*threading_height+ring_gear_number_stages*(3*gear_height)+gear_height/2]) translate([0,0,1.5*gear_height]) rotate([180,0,0]) radial_ball_bearing_upper_cap(3/2*gear_height);

        // Generate carrier, planet gears and thrust ball bearings by number of stages
        for(number_of_stages = [0:1:ring_gear_number_stages-1])
        {
            // Offset parts by number of stages
            translate([0,0,number_of_stages*(3*gear_height)])
            {
                // Generate carrier
                translate([0,0,nema_stepper_shaft_height-1/2*gear_height]) carrier();
                // Generate planet gears
                translate([0,0,nema_stepper_shaft_height+1/2*gear_height]) planet_gear();
                // Generate thrust ball bearing
                translate([0,0,nema_stepper_shaft_height-3/2*gear_height]) ball_bearing();
                // Generate thrust ball bearing cage
                translate([0,0,nema_stepper_shaft_height-3/2*gear_height]) ball_bearing_cage();
            }
        }

        // Generate thrust ball bearing
        translate([0,0,nema_stepper_shaft_height-3/2*gear_height+ring_gear_number_stages*(3*gear_height)]) ball_bearing();
        // Generate thrust ball bearing cage
        translate([0,0,nema_stepper_shaft_height-3/2*gear_height+ring_gear_number_stages*(3*gear_height)]) ball_bearing_cage();

        // Generate sun gear carrier by number of stages
        for(number_of_stages = [0:1:ring_gear_number_stages-2])
        {
            // Offset parts by number of stages
            translate([0,0,number_of_stages*(3*gear_height)])
            {
                // Generate sun gear carrier
                translate([0,0,nema_stepper_shaft_height+gear_height]) sun_gear_carrier();
            }
        }

        // Generate output shaft carrier
        translate([0,0,gear_height+(ring_gear_number_stages-1)*(3*gear_height)]) translate([0,0,nema_stepper_shaft_height]) output_shaft();
    }

    else if (view_mode == 2)
    {
        // Expanded view spacing
        spacer = 3*gear_height;

        // Generate stepper motor sun gear
        nema_gear();

        // Generate bottom ball bearing cap
        color("PaleVioletRed") radial_ball_bearing_bottom_cap(3/2*gear_height);

        // Generate nema motor attachment
        color("DodgerBlue") translate([0,0,-nema_motor_plinth_height(size=nema_stepper_size)]) nema_motor_plate();

        // Generate gearbox case
        color("CornflowerBlue") translate([0,0,nema_stepper_shaft_height/2+(ring_gear_number_stages+4)*spacer]) gearbox_case();

        // Generate gearbox cap
        color("RoyalBlue") translate([0,0,nema_stepper_shaft_height/2+ring_gear_number_stages*(3*gear_height)+(ring_gear_number_stages+5)*spacer]) gearbox_cap();

        // Generate upper ball bearing cap
        color("Salmon")  translate([0,0,nema_stepper_shaft_height/2+ring_gear_number_stages*(3*gear_height)+(ring_gear_number_stages+5)*spacer]) translate([0,0,1.5*gear_height]) rotate([180,0,0]) radial_ball_bearing_upper_cap(3/2*gear_height);

        // Generate carrier and planet gears by number of stages
        for(number_of_stages = [0:1:ring_gear_number_stages-1])
        {
            // Offset parts by number of stages
            translate([0,0,number_of_stages*(3*gear_height)])
            {
                // Generate carrier
                color("FireBrick") translate([0,0,gear_height/2+(number_of_stages+1.1)*spacer]) carrier();
                // Generate planet gears
                color("Lime") translate([0,0,nema_stepper_shaft_height-gear_height/2+(number_of_stages+1.2)*spacer]) planet_gear();
            }
        }

        // Generate thrust ball bearings by number of stages
        for(number_of_stages = [0:1:ring_gear_number_stages-1])
        {
            // Offset parts by number of stages
            translate([0,0,number_of_stages*(3*gear_height)])
            {
                // Generate thrust ball bearing
                color("DarkSeaGreen")translate([0,0,(number_of_stages+1)*spacer]) ball_bearing();
                // Generate thrust ball bearing cage
                color("ForestGreen") translate([0,0,(number_of_stages+1)*spacer]) ball_bearing_cage();
            }
        }

        // Generate sun gear carrier by number of stages
        for(number_of_stages = [0:1:ring_gear_number_stages-2])
        {
            // Offset parts by number of stages
            translate([0,0,number_of_stages*(3*gear_height)])
            {
                // Generate sun gear carrier
                color("BlueViolet") translate([0,0,nema_stepper_shaft_height+(number_of_stages+1.39)*spacer]) sun_gear_carrier();
            }
        }

        // Generate output shaft carrier
        color("RosyBrown") translate([0,0, (ring_gear_number_stages-1)*(3*gear_height)+nema_stepper_shaft_height+(ring_gear_number_stages+0.4)*spacer]) output_shaft();

        // Generate output ball bearing
        color("DarkCyan") translate([0,0, (ring_gear_number_stages-1)*(3*gear_height)+nema_stepper_shaft_height+(ring_gear_number_stages+0.4)*1.25*spacer]) output_ball_bearing();

        // Generate output ball bearing cage
        color("Cyan") translate([0,0, (ring_gear_number_stages-1)*(3*gear_height)+nema_stepper_shaft_height+(ring_gear_number_stages+0.4)*1.25*spacer]) output_ball_bearing_cage();
    }
}
