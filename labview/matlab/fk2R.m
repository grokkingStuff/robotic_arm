function [j01, j02] = fk2R(th1, th2, l1, l2)
x1 = l1*cos(th1);
y1 = l1*sin(th1);
xE = l1*cos(th1)+l2*cos(th2);
yE = l1*sin(th1)+l2*sin(th2);
j01 = [x1; y1];
j02 = [xE; yE];
end

