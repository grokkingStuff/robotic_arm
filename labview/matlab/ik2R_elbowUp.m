function [theta1, theta2] = ik2R_elbowUp(xE, l1, l2)
    x1 = xE(1,:);
    y1 = xE(2,:);
    D = (x1.^2 + y1.^2 - l1.^2 - l2.^2)./(2 * l1 * l2);
    theta2 = -atan2(sqrt(1 - D.^2), D);
    theta1 = atan2(y1,x1)-atan2(l2 .* sin(theta2), l1 + l2 .* cos(theta2));

end