function [q, v, a] = quintic(q0, v0, a0, t0, qf, vf, af, tf)     
    % 6x6 t matrix
    T = [1, t0, t0^2, t0^3, t0^4, t0^5;
         0, 1, 2*t0, 3*t0^2, 4*t0^3, 5*t0^4;
         0, 0, 2, 6*t0, 12*t0^2, 20*t0^3;
         1, tf, tf^2, tf^3, tf^4, tf^5;
         0, 1, 2*tf, 3*tf^2, 4*tf^3, 5*tf^4;
         0, 0, 2, 6*tf, 12*tf^2, 20*tf^3];
    
    % 6x1 matrix for initial and final conditions
    Q = [q0; v0; a0; qf; vf; af];
    
    % 6x1 coefficient matrix
    C = inv(T)*Q;

    q = [];
    v = [];
    a = [];
    % iterate through number of time steps
    for t=t0:tf
        q = [q, (C(1) + C(2)*t + C(3)*t^2 + C(4)*t^3 + C(5)*t^4 + C(6)*t^5)];
        v = [v, (C(2) + 2*C(3)*t + 3*C(4)*t^2 + 4*C(5)*t^3 + 5*C(6)*t^4)];
        a = [a, (2*C(3) + 6*C(4)*t + 12*C(5)*t^2 + 20*C(6)*t^3)];
    end
 end